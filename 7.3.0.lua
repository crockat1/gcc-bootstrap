-- GNU

local pkg_info = {}

pkg_info["name"] = "gcc"
pkg_info["version"] = "7.3.0"
pkg_info["installed_by"] = "Michael M. Crockatt"
pkg_info["installation_date"] = "2018-02-24"
pkg_info["func_areas"] = {
    "Compilers:: C",
    "Compilers:: C++",
    "Compilers:: Fortran 90/95/2003",
    "Parallel Programming:: OpenMP"
}
pkg_info["vendors"] = { "Free Software Foundation" }
pkg_info["programs"] = {
    "ar",
    "as",
    "gcc",
    "g++",
    "gcov",
    "gfortran",
    "gprof",
    "ld",
    "nm",
    "objcopy",
    "objdump",
    "readelf"
}
pkg_info["libraries"] = {
    "-lstdc++",
    "-lgfortran"
}

-- Assumes that software is installed in ${HOME}/software/
HOME_PATH = os.getenv("HOME")
HOME_SOFTWARE_PATH = pathJoin(HOME_PATH, "software")
HOME_MODULE_PATH = pathJoin(HOME_SOFTWARE_PATH, "modulefiles")

local base_path = HOME_SOFTWARE_PATH

-- Base directory for package is ${HOME}/software/<package name>/<version number>/.
base_path = pathJoin(base_path, pkg_info["name"])
base_path = pathJoin(base_path, pkg_info["version"])

-- Package is installed in install/ subdirectory of base direction.
local install_path = pathJoin(base_path, "install")

-- Set path environment variables for executables, libraries, and headers.
prepend_path("PATH",            pathJoin(install_path, "bin"))
prepend_path("LD_LIBRARY_PATH", pathJoin(install_path, "lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(install_path, "lib64"))
prepend_path("LIBRARY_PATH",    pathJoin(install_path, "lib"))
prepend_path("LIBRARY_PATH",    pathJoin(install_path, "lib64"))
prepend_path("CPATH",           pathJoin(install_path, "include"))

-- Set compiler environment variables.
setenv("CC",  ""..install_path.."/bin/gcc")
setenv("CXX", ""..install_path.."/bin/g++")
setenv("FC",  ""..install_path.."/bin/gfortran")

-- Add include directories to compiler flag environment variables.
prepend_path("CFLAGS",   "-I" .. pathJoin(install_path, "include"), " ")
prepend_path("CPPFLAGS", "-I" .. pathJoin(install_path, "include"), " ")
prepend_path("FFLAGS",   "-I" .. pathJoin(install_path, "include"), " ")

-- Add path for modules that depend on compiler; i.e., software installed into:
--
--      ${HOME}/software/<package name>/<version number>/software/
--
prepend_path("MODULEPATH", pathJoin(base_path, "modulefiles"))
