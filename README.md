This repository contains a collection of scripts to perform a manual combined build of GCC and binutils. The motivation for doing so is [simple](https://mailman-1.sys.kth.se/pipermail/gromacs.org_gmx-users/2016-January/103146.html):

> The "stable" compilers in distros like centos are merely a euphemism for outdated, if you want to run fast on new hardware.

CentOS 6.8 is currently used at MSU's HPCC, which is based on GCC 4.4.7 (released 13-Mar-2012) and binutils 2.20 (released 26-Aug-2011). These software versions were released several years before the processors in the intel16 cluster: Broadwell Xeons were released Q1 2016. The result is that the system default assembler does not support the newest instructions sets, such as AVX2, that the Broadwell processors support. If one complies GCC by itself, then the newer compiler will still use the system default assembler. A combined build of newer versions of GCC and binutils will provide support for the most recent instruction sets available as well as improved compiler optimization implementations.

This guide is based on the instructions found [here](https://stackoverflow.com/questions/1726042/recipe-for-compiling-binutils-gcc-together/6228588#6228588).

# Build Process

The build process is composed of four steps:

1.  Unpack and link sources.
1.  Configure the build.
1.  Compile.
1.  Install.

Scripts are provided for assisting with the first two steps.

This guide is organized around the following directory structure. Given some base directory, which we denote by `.`, three subdirectories are used:

-   `./src`
-   `./build`
-   `./install`

The `./src` subdirectory contains the source files for the compiler and utilities. The `./build` directory is the location in which the build is executed. The executables and libraries resulting from the build are installed into `./install`. The remainder of this guide assumes the following directory structure:

```
.
├── 7.3.0.lua
├── build
├── configure.sh
├── install
├── README.md
├── setup-sources.sh
└── src
```

### Step 1: Unpack and link sources

The first step is to unpack and link the sources. The script `setup-sources.sh` has been provided to automate this process. This script should be executed from the `./src` directory; i.e.,
```bash
cd ./src
../setup-sources.sh
```
This script executes two tasks:

1.  Download and unpack all sources.
1.  Link all sources for dependencies into the GCC source tree.

The dependencies are as follows:

- binutils
- gmp
- mpc
- mpfr
- isl

Appropriate versions for each of these packages will need to be set inside of the `setup-sources.sh` script before executing it. One would generally select the version of GCC that they wish to compile, and then select versions for the remaining packages that are compatible with the desired version of GCC.

It is important to note that GCC and binutils are formally separate packages that are developed in parallel. Because of this, the compatibility between different versions of these packages varies over time. Selecting compatible versions is not always straightforward and can involve some trial and error. If one tries to compile using versions of binutils and GCC that are not compatible, a compilation error will usually result. A reasonable way to go about finding a set of compatible versions is to:

1.  Select some initial set of versions.
1.  Try incrementing/decrementing the version of binutils used until the build is successful.

The initial set of versions can be selected by taking several things into account:

- Since GCC and binutils are, roughly speaking, developed in parallel, selecting versions released around the same time is probably a good choice.
- Various linux distributions are usually built on versions that are believed to be more or less compatible. One could look and see which versions of GCC and binutils are used for the releases of major distributions, e.g., [Ubuntu](https://distrowatch.com/table.php?distribution=ubuntu). It should be noted that distributions may apply their own patches that can affect compatibility, so the versions used may not directly correspond to ideal choices.

The initial guess does not need to be perfect, since ultimately you'll just need to try and see if it works.

### Step 2: Configure the build

The second step is to configure the build. This is done using the standard GNU Autotools build system. The script `configure.sh` will execute the configuration, and configuration options can be set in this file as desired. To perform the configuration using this script, execute `../configure.sh` from within the `./build/` subdirectory.

### Steps 3 & 4: Compile and install

Once the configuration is complete, the code can be compiled and installed. Compilation is done by executing `make -j 4` in the `./build/` directory, where 4 can be replaced with the desired number of parallel compilation processes for your system. Note that the compilation step will take some time, even on high-performance systems.

If the compilation fails, you can try to change the version of binutils used. To try a different version, change to the base directory `.`, remove the source and build directories:

```bash
rm -rf ./src/ ./build/
```

and then start back at the first step with the desired modifications to `setup-sources.sh`.

Once the compilation completes successfully, install the compiler by executing `make install` from the `./build/` subdirectory.

# Using the New Compiler

To use the newly-built compiler, the libraries, headers, and executables in the install directory must be added to your environment. If `<install-base>` is the full pathname of `./install/`, then this can be done by exporting the following environment variables:

-   Add the `bin` directory to your `PATH` variable so that your shell can find the compiler executables:
    ```bash
export PATH=<install-base>/bin:${PATH}
    ```

-   Add the `lib` and `lib64` directories to your `LIBRARY_PATH` and `LD_LIBRARY_PATH` so that libraries can be found during compilation and execution, respectively, of programs compiled with the new compiler:
    ```bash
export LIBRARY_PATH=<install-base>/lib:${LIBRARY_PATH}
export LIBRARY_PATH=<install-base>/lib64:${LIBRARY_PATH}
export LD_LIBRARY_PATH=<install-base>/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=<install-base>/lib64:${LD_LIBRARY_PATH}
    ```

-   Add the `include` directory to your `CPATH` variable so that new headers can be found:
    ```bash
export CPATH=<install-base>/include:${CPATH}
    ```

A sample modulefile for the lmod modules system (7.3.0.lua) is provided for interested users.
