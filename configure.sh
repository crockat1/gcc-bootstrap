#!/bin/bash
#
# Configure build.
#

../src/gcc-*/configure \
    --prefix=$( dirname $(pwd) )/install \
    --enable-languages=c,c++,fortran \
    --disable-multilib
