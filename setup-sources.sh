#!/bin/bash
#
# Download sources and setup directory tree for concurrent build of GNU toolchain components.
#
# Adjust version numbers below as necessary.
#


#
# Download GNU sources.
#
# The link ftpmirror.gnu.org will automatically redirect to an official GNU mirror, which vary significantly
# in quality. The complexity of the download implementation below is the result of attempts to increase the
# robustness of the download process for mirrors that are less well behaved. In practice, using the kernel.org
# mirror is a much better option.
#
#GNU_MIRROR="http://ftpmirror.gnu.org"
GNU_MIRROR="http://mirrors.kernel.org"

for package in "gcc/gcc-7.3.0/gcc-7.3.0" "binutils/binutils-2.28.1" "gmp/gmp-6.1.2" "mpc/mpc-1.0.3" "mpfr/mpfr-3.1.6"
do
    while [ 1 ]
    do
        # Try different packagename extensions, in order of preference.
        for ext in bz2 gz xz
        do
            # Try download.
            wget \
                --continue \
                --retry-connrefused \
                --waitretry=1 \
                --read-timeout=5 \
                --timeout=5 \
                -t 0 \
                "${GNU_MIRROR}/gnu/${package}.tar.${ext}"

            # Check return value, break if successful (0).
            if [ $? = 0 ]; then break 2; fi
        done

        # Sleep and retry on failure.
        sleep 1s
    done
done

# Download isl: SET VERSION HERE
wget \
    --continue \
    --retry-connrefused \
    --waitretry=1 \
    --read-timeout=5 \
    --timeout=5 \
    -t 0 \
    ftp://gcc.gnu.org/pub/gcc/infrastructure/isl-0.18.tar.bz2

# Extract all archives.
for tar in *.tar.*
do
    echo "Extracting ${tar}..."
    tar xf "${tar}"
done
echo -e "Extraction complete.\n"

# Determine directory for GCC sources.
gccdir=$( find . -maxdepth 1 -type d -name 'gcc*' )

# Link standard dependencies into the GCC source tree.
for lib in gmp mpc mpfr isl
do
    libdir=$( find . -maxdepth 1 -type d -name "${lib}*" -printf "%f" )
    echo $libdir
    ln -sv ../${libdir} ${gccdir}/${libdir%-[0-9.]*}
done

# Link components of binutils into the GCC source tree.
cd ${gccdir}

for thing in ../binutils-*/*
do
    ln -sv "${thing}"
done
